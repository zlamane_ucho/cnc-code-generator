let button = document.getElementById("generate-button");

button.addEventListener('click', () => {
    let code = document.getElementById("cone-type").value === 'true' ?
    generateOuterCone(getInputsData()):generateInnerCone(getInputsData());

    document.getElementById('generated-code').innerHTML = code;
});


getInputsData = () => {
    let data = {
        z: null,
        start: null,
        end: null,
        angle: null,
        feed: null,
        correction: null,
        interpolation: null,
    };

    data.z = parseFloat(document.getElementById("z-axis-step").value);
    data.start = parseFloat(document.getElementById("start-diameter").value);
    data.end = parseFloat(document.getElementById("end-diameter").value);
    data.angle = parseFloat(document.getElementById("angle").value);
    data.feed = parseFloat(document.getElementById("feedrate").value);
    data.correction = document.getElementById("diameter-correction").value;
    data.interpolation = document.getElementById("interpolation").value;
    data.outerCone = (document.getElementById("cone-type").value === 'true');

    return data;
};

generateInnerCone = (data) => {
    let xAxisStep = Math.round(data.z * Math.tan(degToRad(data.angle/2)) * 1000) / 1000;
    let xPosition = data.start/2;
    
    let code = `G01 G91 ${data.correction} X-${xPosition.toFixed(3)} F${data.feed}.;<br>`;
    
    do {
        code += `${data.interpolation} I${xPosition.toFixed(3)} J0 Z-${data.z};<br>`;
        code += `G01 X${xAxisStep.toFixed(3)};<br>`;
        xPosition -= xAxisStep;
    } while (xPosition > data.end/2 - xAxisStep - 0.1)

    code += `G01 G40 X${xPosition.toFixed(3)};<br>`
    code += `M99;`

    return code;
};

generateOuterCone = (data) => {
    let xAxisStep = Math.round(data.z * Math.tan(degToRad(data.angle/2)) * 1000) / 1000;
    let xPosition = data.start/2;
    let endCondition = null;
    
    let code = `G01 G90 ${data.correction} X-${xPosition.toFixed(3)} F${data.feed}.;<br>G91 `;
    
    do {
        code += `${data.interpolation} I${xPosition.toFixed(3)} J0 Z-${data.z};<br>`;
        code += `G01 X${xAxisStep.toFixed(3)};<br>`;
        xPosition += xAxisStep;
    } while (xPosition < data.end/2 + xAxisStep + 0.1)

    code += `M99;`

    return code;
};


generateCode = (data) => {
    let xAxisStep = Math.round(data.z * Math.tan(degToRad(data.angle/2)) * 1000) / 1000;
    let xPosition = data.start/2;
    let endCondition = null;
    
    let code = `G01 
    ${data.outerCone ? "G90" : "G91"} 
    ${data.correction} 
    X-${xPosition.toFixed(3)} 
    F${data.feed}.;<br>
    ${data.outerCone ? "G91 " : ""}`;
    
    do {
        code += `${data.interpolation} I${xPosition.toFixed(3)} J0 Z-${data.z};<br>`;
        code += `G01 X${xAxisStep.toFixed(3)};<br>`;
        data.outerCone ? xPosition += xAxisStep : xPosition -= xAxisStep;
        endCondition = data.outerCone ? (xPosition < data.end/2 + xAxisStep) : (xPosition > data.end/2 - xAxisStep)

    } while (endCondition)

    if(data.outerCone) code += `G01 G40 X-${xPosition.toFixed(3)};<br>`;
    else code += `G01 G40 X${xPosition.toFixed(3)};<br>`
    code += `M99;`

    return code;
}

degToRad = (deg) => Math.PI * deg / 180;
